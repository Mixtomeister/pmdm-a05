package com.example.mylibrary.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mylibrary.R;
import com.example.mylibrary.async.JSONHttpReader;
import com.example.mylibrary.async.JSONHttpReaderListener;
import com.example.mylibrary.events.EventsAdmin;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeatherFragment extends Fragment implements JSONHttpReaderListener {
    private TextView txtLocalidad, txtTemp, txtTempMin, txtTempMax, txtDescription;
    private JSONHttpReader jsonReader;

    public WeatherFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_weather, container, false);
        txtLocalidad = v.findViewById(R.id.localidad);
        txtTemp = v.findViewById(R.id.temperatura);
        txtTempMin = v.findViewById(R.id.tempMin);
        txtTempMax = v.findViewById(R.id.tempMax);
        txtDescription = v.findViewById(R.id.description);
        this.jsonReader = new JSONHttpReader();
        this.jsonReader.setListener(this);
        this.jsonReader.execute("http://api.openweathermap.org/data/2.5/weather?q=Madrid,ES&appid=25a97a141e7e83d3eb1e9b34aea27985");
        return v;
    }


    @Override
    public void jsonResponse(String json) {
        Gson gson = new GsonBuilder().create();
        Weather tiempo = gson.fromJson(json, Weather.class);
        updateWeather(tiempo);
    }

    private void updateWeather(Weather tiempo){
        txtLocalidad.setText(tiempo.getName());
        txtTemp.setText("Temperatura: " + ((int)(tiempo.getMain().get("temp") - 273)) + "ºC");
        txtTempMin.setText("Temperatura mínima: " + ((int)(tiempo.getMain().get("temp_min") - 273)) + "ºC");
        txtTempMax.setText("Temperatura máxima: " + ((int)(tiempo.getMain().get("temp_max") - 273)) + "ºC");
        txtDescription.setText("Humedad: " + tiempo.getMain().get("humidity") + "%");
    }
}

class Weather{
    private HashMap<String, Double> main;
    private String name;

    public Weather() {
    }

    public Weather(HashMap<String, Double> main, String name) {
        this.main = main;
        this.name = name;
    }

    public HashMap<String, Double> getMain() {
        return main;
    }

    public void setMain(HashMap<String, Double> main) {
        this.main = main;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
