package com.example.mylibrary.async;

public interface JSONHttpReaderListener {
    void jsonResponse(String json);
}
