package com.example.mylibrary.async;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JSONHttpReader extends AsyncTask<String, Integer, String> {
    private JSONHttpReaderListener listener;

    @Override
    protected String doInBackground(String... urls) {
        try {
            URL jsonUrl = new URL(urls[0]);
            String response = "";
            HttpURLConnection conx = (HttpURLConnection) jsonUrl.openConnection();
            conx.setRequestMethod("GET");
            conx.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(conx.getInputStream()));
            String str = "";
            while((str=br.readLine())!=null){
                response += str;
            }
            return response;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        this.listener.jsonResponse(s);
    }

    public void setListener(JSONHttpReaderListener listener) {
        this.listener = listener;
    }
}
