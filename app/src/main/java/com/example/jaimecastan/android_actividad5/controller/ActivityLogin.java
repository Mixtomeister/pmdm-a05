package com.example.jaimecastan.android_actividad5.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.jaimecastan.android_actividad5.R;
import com.example.mylibrary.async.JSONHttpReader;
import com.example.mylibrary.async.JSONHttpReaderListener;
import com.example.mylibrary.events.EventsAdmin;
import com.example.mylibrary.events.EventsListener;
import com.example.mylibrary.fragments.LoginFragment;
import com.example.mylibrary.fragments.RegisterFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ActivityLogin extends AppCompatActivity implements EventsListener, JSONHttpReaderListener{
    private View snack;
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;
    private String action;
    private JSONHttpReader jsonReader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.loginFragment);
        registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentById(R.id.registerFragment);

        EventsAdmin.getInstance().addListener(this);

        onReturnClicker();
    }

    public void onLoginClicked(View view) {
        if (!loginFragment.getEdtxt_User().getText().toString().equals("") && !loginFragment.getEdtxt_Password().getText().toString().equals("")) {
            this.action = "Login";
            this.jsonReader = new JSONHttpReader();
            this.jsonReader.setListener(this);
            this.jsonReader.execute(String.format("http://192.168.1.128/basketApp/login.php?usuario=%s&pass=%s",
                    loginFragment.getEdtxt_User().getText().toString(), loginFragment.getEdtxt_Password().getText().toString()));
        } else {
            Snackbar.make(view, "Fill the sign in fields", Snackbar.LENGTH_SHORT).show();
        }

    }

    public void onNewRegisterClicker(View view) {
        if (!registerFragment.getEdtxt_NewUser().getText().toString().equals("") && !registerFragment.getEdtxt_NewPassword().getText().toString().equals(""))
            if (registerFragment.getEdtxt_NewPassword().getText().toString().equals(registerFragment.getEdtxt_NewPassword2().getText().toString())) {
                loginFragment.getEdtxt_User().setText(registerFragment.getEdtxt_NewUser().getText().toString());
                this.action = "Register";
                this.jsonReader = new JSONHttpReader();
                this.jsonReader.setListener(this);
                this.jsonReader.execute(String.format("http://192.168.1.128/basketApp/register.php?usuario=%s&pass=%s",
                        registerFragment.getEdtxt_NewUser().getText().toString(), registerFragment.getEdtxt_NewPassword().getText().toString()));
            } else Snackbar.make(view, "The passwords have to match", Snackbar.LENGTH_SHORT).show();
        else
            Snackbar.make(view, "Please fill all the fields.", Snackbar.LENGTH_SHORT).show();
    }

    public void onRegisterClicked(View view) {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(loginFragment);
        transition.show(registerFragment);
        transition.commit();
        clearLoginFragment();
    }

    public void onReturnClicker() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(registerFragment);
        transition.show(loginFragment);
        transition.commit();
        clearRegisterFragment();
    }

    public void clearLoginFragment() {
        loginFragment.getEdtxt_User().setText("");
        loginFragment.getEdtxt_Password().setText("");
    }

    public void clearRegisterFragment() {
        registerFragment.getEdtxt_NewUser().setText("");
        registerFragment.getEdtxt_NewPassword().setText("");
        registerFragment.getEdtxt_NewPassword2().setText("");
    }

    @Override
    public void onClickScren(View view) {
        snack = view;
        if (view.getId() == R.id.btnLogin) {
            onLoginClicked(view);
        } else if (view.getId() == R.id.btnRegister)
            onRegisterClicked(view);
        else if (view.getId() == R.id.btnNRegister) {
            onNewRegisterClicker(view);
        } else if (view.getId() == R.id.btnVolver)
            onReturnClicker();
    }

    @Override
    public void jsonResponse(String json) {
        Gson gson = new GsonBuilder().create();

        RequestStatus status = gson.fromJson(json, RequestStatus.class);
        if(!status.getState().equals("OK")){
            Snackbar.make(snack, "An error has occurred in " + this.action, Snackbar.LENGTH_SHORT).show();
        }else{
            if(this.action.equals("Login")){
                userSignedIn();
            }else if(this.action.equals("Register")){
                Snackbar.make(snack, "User created", Snackbar.LENGTH_SHORT).show();
                userCreated();
            }
        }
    }

    public void userSignedIn() {
        Intent secondActivity = new Intent(this, MainActivity.class);
        startActivity(secondActivity);
    }

    public void userCreated() {
        onReturnClicker();
        clearRegisterFragment();
    }

}

class RequestStatus{
    private String state;

    public RequestStatus() {
    }

    public RequestStatus(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
