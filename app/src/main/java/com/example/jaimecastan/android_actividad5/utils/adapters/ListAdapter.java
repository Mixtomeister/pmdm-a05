package com.example.jaimecastan.android_actividad5.utils.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.jaimecastan.android_actividad5.R;
import com.example.jaimecastan.android_actividad5.model.instance.DataHolder;
import com.example.jaimecastan.android_actividad5.utils.cells.CeldaJugador;

public class ListAdapter extends RecyclerView.Adapter<CeldaJugador> {
    private Context context;

    public ListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CeldaJugador onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_prototype,null);
        CeldaJugador celda = new CeldaJugador(view);
        return celda;
    }

    @Override
    public void onBindViewHolder(CeldaJugador celda, int position) {
        Log.v("ActivityList","Siiii lleggaaa------------------------------------->" + DataHolder.getInstance().getJugadores().get(position).getNombre());
        celda.setNombreJugador(DataHolder.getInstance().getJugadores().get(position).getNombre());
        celda.setNombreEquipo(DataHolder.getInstance().getJugadores().get(position).getEquipo());
        celda.setAlturaJugador(String.valueOf(DataHolder.getInstance().getJugadores().get(position).getAltura()) + " cm");
       // celda.setImagenJugador(DataHolder.getInstance().getJugadores().get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return DataHolder.getInstance().getJugadores().size();
    }
}
