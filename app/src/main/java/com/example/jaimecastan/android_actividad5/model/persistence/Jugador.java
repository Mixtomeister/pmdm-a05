package com.example.jaimecastan.android_actividad5.model.persistence;

public class Jugador {
    private String nombre;
    private String equipo;
    private Double altura;

    public Jugador() {

    }

    public Jugador(String nombre, String equipo, Double altura) {
        this.nombre = nombre;
        this.equipo = equipo;
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

}
