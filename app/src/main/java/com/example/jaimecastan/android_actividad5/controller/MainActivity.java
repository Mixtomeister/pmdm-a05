package com.example.jaimecastan.android_actividad5.controller;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.jaimecastan.android_actividad5.R;
import com.example.jaimecastan.android_actividad5.model.instance.DataHolder;
import com.example.jaimecastan.android_actividad5.model.persistence.Jugador;
import com.example.mylibrary.async.JSONHttpReader;
import com.example.mylibrary.async.JSONHttpReaderListener;
import com.example.mylibrary.fragments.ListFragment;
import com.example.mylibrary.fragments.WeatherFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.example.jaimecastan.android_actividad5.utils.adapters.ListAdapter;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity implements JSONHttpReaderListener {
    private WeatherFragment weatherFragment;
    private ListFragment listFragment;
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weatherFragment = (WeatherFragment) getSupportFragmentManager().findFragmentById(R.id.tiempoFrag);
        listFragment = (ListFragment) getSupportFragmentManager().findFragmentById(R.id.listFragment);

        JSONHttpReader jsonReader = new JSONHttpReader();
        jsonReader.setListener(this);
        jsonReader.execute("http://192.168.1.128/basketApp/getJugadores.php");

        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(listFragment);
        transition.show(weatherFragment);
        transition.commit();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleFragment();
            }
        });
    }

    @Override
    public void jsonResponse(String json) {
        Gson gson = new GsonBuilder().create();
        DataHolder.getInstance().setJugadores((ArrayList<Jugador>) gson.fromJson(json, new TypeToken<ArrayList<Jugador>>(){}.getType()));
        listFragment.getRecyclerView().setAdapter(new ListAdapter(this));
    }

    public void toggleFragment() {
        if(flag){
            FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
            transition.hide(listFragment);
            transition.show(weatherFragment);
            transition.commit();
        }else{
            FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
            transition.hide(weatherFragment);
            transition.show(listFragment);
            transition.commit();
        }
        flag = !flag;
    }
}
