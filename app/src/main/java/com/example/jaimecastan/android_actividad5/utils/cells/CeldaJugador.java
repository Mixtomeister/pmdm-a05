package com.example.jaimecastan.android_actividad5.utils.cells;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.jaimecastan.android_actividad5.R;

public class CeldaJugador extends RecyclerView.ViewHolder {
    private TextView nombreJugador;
    private TextView equipoJugador;
    private TextView alturaJugador;

    public CeldaJugador(View itemView) {
        super(itemView);
        nombreJugador = itemView.findViewById(R.id.nombreJugador);
        equipoJugador = itemView.findViewById(R.id.equipoJugador);
        alturaJugador = itemView.findViewById(R.id.alturaJugador);
    }

    public void setNombreJugador(String nombre) {
        this.nombreJugador.setText(nombre);
    }

    public void setNombreEquipo(String equipo) {
        this.equipoJugador.setText(equipo);
    }

    public void setAlturaJugador(String nombre) {
        this.alturaJugador.setText(nombre);
    }
}
