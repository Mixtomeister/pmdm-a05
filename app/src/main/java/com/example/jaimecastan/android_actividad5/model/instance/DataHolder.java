package com.example.jaimecastan.android_actividad5.model.instance;

import com.example.jaimecastan.android_actividad5.model.persistence.Jugador;
import java.util.ArrayList;

public class DataHolder {
    private static DataHolder instance = new DataHolder();
    private ArrayList<Jugador> jugadores;

    public DataHolder() {
        jugadores = new ArrayList();
    }

    public static DataHolder getInstance() {
        return instance;
    }

    public ArrayList<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(ArrayList<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

}
